<a href='https://www.mattkeeter.com/projects/kokopelli/'>Kokopelli</a> is an old (but great) project from <a href='https://www.mattkeeter.com/'>Matt Keeter</a>.  Its a full-blown CAD program, but it can be used for designing PCBs.  The programmatic interface makes it easy to set up relationships between components, traces, etc. and you can toolpath for the circuit mill right in kokopelli.

To use it, clone it from github: https://github.com/mkeeter/kokopelli and install with the makefile.

I've added a couple small utilities to Matt's original PCB design library.  If you'd like to use them, you can replace the existing koko/lib/pcb.py with <a href='pcb.py'>pcb.py</a> and reinstall.

A fun example using several of the included features is a stepper driver board I designed:

<img src='servo-stepper-v1-layout.png' width=500px>


[pcb.py](pcb.py): FRep PCB design  
[frep.py](frep.py): pure Python FRep renderer  
[frep-C.py](frep-C.py): faster Python+C FRep renderer  
[frep_gpu.py](https://gitlab.fabcloud.org/pub/project/frep_gpu.py): faster GPU renderer  
[gerber plot](https://modsproject.org/?program=programs/formats/gerber%20plot): image to Gerber converter  

board:  
![](images/pcb.png)  
top:  
![](images/top.png)  
bottom:  
![](images/bottom.png)  
mask:  
![](images/mask.png)  
holes:  
![](images/holes.png)  
interior:  
![](images/interior.png)  
text:  
![](images/text.png)  
Gerber:  
![](Gerber/Gerber.png)  
